import java.util.ArrayList;

public class PolyGraph {
	static ArrayList<Polygon> Polygon = new ArrayList<Polygon>();
	
	public static void addPolygon(Polygon polygon){
		Polygon.add(polygon);
	}
	
	public static void copyMap(ArrayList<Polygon> polygon){
		Polygon = (ArrayList<Polygon>) polygon.clone();
	}
	
	public static Polygon getPolygon(int index){
		return Polygon.get(index);
	}
	
	public static int numberOfPolygons(){
		return Polygon.size();
	}
}
