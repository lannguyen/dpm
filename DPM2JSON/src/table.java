import java.text.DecimalFormat;

public class table {
	double[][] result;
	public table(){
		result = new double[PolyGraph.numberOfPolygons()][PolyGraph.numberOfPolygons()];
		for(int i = 0; i < result.length; i++)
		{
			for(int j = 0; j < result.length; j++)
			{
				if(i==j)
					result[i][j]=0;
				else
					result[i][j]=PolyGraph.getPolygon(i).findDistance(PolyGraph.getPolygon(j));
			}
		}
	}
	@Override
	public String toString()
	{
		String output = "";
		for(int i = 0; i < result.length; i++)
		{
			for(int j = 0; j < result.length; j++)
			{
				output += new DecimalFormat("#0.00").format(result[i][j])+"  ";
			}
			output +="\n";	
		}
		return output;
	}
}
