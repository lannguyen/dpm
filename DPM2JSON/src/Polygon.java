/*
 * This class create instances of polygons
 * providing centroid of the polygon calculation
 * Algorithm of centroid can be fine here:https://en.wikipedia.org/wiki/Centroid#Centroid_of_polygon
 */

import java.util.ArrayList;
import java.util.Collections;

public class Polygon
{
	ArrayList<Double> lat;
	ArrayList<Double> lon;
	int number;
	double Clat = 0;
	double Clon = 0;
	public Polygon(ArrayList<Double> lat, ArrayList<Double> lon, int number){
		this.lat = lat;
		this.lon = lon;
		this.number = number;
		calculateCentroid();
	}

	public Polygon(double xCoordinate,double yCoordinate, int number){
		Clat = (double)xCoordinate;
		Clon = (double)yCoordinate;
		this.number = number;
		//getCentroid();
	}
	
	// computer centroid, result value is a list where slot 0 = centroiX, 1 = centroidY
	public void calculateCentroid()
	{
		Collections.sort(lat);   
		Collections.sort(lon);    
		
		double y1 = lat.get(0);
		double y2 = lat.get(lat.size()-1);
		double x1 = lon.get(0);
		double x2 = lon.get(lon.size()-1);
		Clat = x1 + ((x2 - x1) / 2);
		Clon = y1 + ((y2 - y1) / 2);	
	}
	
	public double[] getCentroid()
	{
		return new double[]{Clon,Clat};
	}

	/*
	 * computer the distance form centroid of this polygon
	 * to target polygon
	 */
	public double findDistance(Polygon target){
		double R = 6372.8;
		double dLat = Math.toRadians(target.Clat - Clat);
        double dLon = Math.toRadians(target.Clon - Clon);
        double seftLat = Math.toRadians(Clat);
        double targetLat = Math.toRadians(target.Clat);
 
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(seftLat) * Math.cos(targetLat);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
	}
	
	@Override
    public String toString(){
        return number+",";
    }
}
