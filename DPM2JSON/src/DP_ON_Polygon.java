import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.io.*;

public class DP_ON_Polygon {
	double [][] distanceTable;
	int size = 0;
	public DP_ON_Polygon(){
		size = PolyGraph.numberOfPolygons();
		distanceTable = new double [size][size];
	}
	
	public ArrayList<Integer> getShortestRoute()
	{
		// fille up the array with the distance between polygon
		// distance of itself will be 0;
		for(int i = 0; i < size; i++){
			for(int j = 0; j< size; j++){
				distanceTable[i][j] = PolyGraph.getPolygon(i).findDistance(PolyGraph.getPolygon(j));
				if(i == j)
					distanceTable[i][j] = -1;
			}
		}
		System.out.println("Finished finding distance");

		ArrayList<Integer> result = new ArrayList<Integer>();
		int row = 0;
		result.add(row);
		while(result.size()!=size){
			row = findMin(distanceTable[row].clone(), result);
			result.add(row);
		}

		return result;
	}
	
	public int findMin(double []arr, ArrayList<Integer> list)
	{
		double [] temp = arr.clone();
		for(int i = 0; i < temp.length; i++)
		{
			if(list.contains(i))
				temp[i] = -1;
		}
		Arrays.sort(temp);
		for(int i = 0; i < temp.length; i++)
		{
			if(temp[i] == -1)
				continue;
			else{
				for (int j = 0; j < arr.length; j++) {
					if(arr[j] == temp[i])
					{
						return j;
					}
				}
			}
		}
		return 0;
	}
}

