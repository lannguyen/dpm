import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
public class WriteReadJson {
	ArrayList <Double> arrLat;
	ArrayList <Double> arrLong;
	JSONArray arr = new JSONArray();
	JSONObject boundaries;
	
	/*
	 * this class is use to parse json file
	 * 
	 */
	public void fixIndex(JSONArray target)
	{	
		for (int i = 0; i < target.size(); i++)
		{
			JSONObject obj = (JSONObject) target.get(i);
			JSONObject properties = (JSONObject) obj.get("properties");
			properties.put("id", i);
		}
	}
	public void out(JSONObject target)
	{	
		String workingDir = System.getProperty("user.dir");
		try {
			File f=new File(workingDir+"/src/data/"+"map_converted.json");  
            f.createNewFile();
			FileWriter file = new FileWriter(f);
			file.write(target.toJSONString());
			System.out.println("Finish Writing");
			file.flush();
			file.close();
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/*
	 * 
	 * user to convert original json to ordered table json object
	 */
	public void out()
	{
		JSONObject obj = new JSONObject();
		obj.put("type", "MultiPolygon");
		JSONArray outer = new JSONArray();
		JSONArray index1 = new JSONArray();
		JSONArray index2 = new JSONArray();
		LinkedList<Double> coordinate = new LinkedList<Double>();
		
		for(int indexPolygon = 0; indexPolygon < PolyGraph.numberOfPolygons(); indexPolygon++)
		{
			int numberOfVertex = PolyGraph.getPolygon(indexPolygon).lat.size();
			index2 = new JSONArray();
			for(int indexCoordinate = 0; indexCoordinate < numberOfVertex; indexCoordinate++)
			{
				coordinate = getCoordinate(indexPolygon,indexCoordinate);
				index2.add(coordinate);
			}
			index1.add(index2);
		}
		outer.add(index1);
		obj.put("coordinates", outer);
		
		String workingDir = System.getProperty("user.dir");
		try {
			File f=new File(workingDir+"/src/data/"+"map.json");  
            f.createNewFile();
			FileWriter file = new FileWriter(f);
			file.write(obj.toJSONString());
			System.out.println("Finish Writing");
			file.flush();
			file.close();
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//indexPolygon refer to the position of a specific polygon in the list
	//indexCoordinate refer to the position of a specific pair of point of the polygon
	private LinkedList<Double> getCoordinate(int indexPolygon, int indexCoordinate)
	{
		LinkedList<Double> coordinate = new LinkedList<Double>();
		Polygon p = PolyGraph.getPolygon(indexPolygon);
		double latitude = p.lat.get(indexCoordinate);
		double longtitude = p.lon.get(indexCoordinate);
		coordinate.add(latitude);
		coordinate.add(longtitude);
		
		return coordinate;
	}
	
	private Polygon parseJsonObject(JSONObject obj)
	{
		JSONObject properties = (JSONObject) obj.get("properties");
		int index = Integer.parseInt((String)properties.get("name"));
		JSONObject geometry = (JSONObject) obj.get("geometry");
		JSONArray arr = (JSONArray) geometry.get("coordinates");
		ArrayList <Double> lat= new ArrayList<Double>();
		ArrayList <Double> lon= new ArrayList<Double>();
		for(int i = 0; i < arr.size(); i++)
		{
			JSONArray inner1 = (JSONArray) arr.get(i);
			for(int j = 0; j < inner1.size(); j++)
			{	
				JSONArray inner2 = (JSONArray) inner1.get(j);
				lon.add( (double) inner2.get(0) );
				lat.add( (double) inner2.get(1) );
			}
		}
		
		Polygon p = new Polygon(lat,lon,index);
		JSONArray centroid = new JSONArray();
		centroid.add(p.getCentroid()[0]);
		centroid.add(p.getCentroid()[1]);
		properties.put("centroid", centroid );
		return p;
	}
	
	public void readFile(String fileName)
	{
		fileName = "map.json";
		JSONParser parser = new JSONParser();
		String workingDir = System.getProperty("user.dir");
		try {
			File f=new File(workingDir+"/src/data/"+"map.json"); 
			Object obj = parser.parse(new FileReader(f));
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray Polygons = (JSONArray) jsonObject.get("features");
			boundaries = (JSONObject) Polygons.get(0);
			for(int i = 1; i < 30; i++)
			{
				arr.add(Polygons.get(i));
			}
			// This method parse the json format to each polygon implementation
			// extracting coordinate and it identity
			// start from 1, skip the boundary
			for (int i = 0; i < arr.size(); i++) {
				JSONObject object = (JSONObject) arr.get(i);
				PolyGraph.addPolygon(parseJsonObject(object));
				
			}
			System.out.println("completed calculation centroid of " + PolyGraph.numberOfPolygons() + " polygon");
			System.out.println("Reading File Completed");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
	
}
