import java.util.ArrayList;
import java.util.Collections;

public class Edge{

    // Holds our Edge of cities
    ArrayList<Polygon> Edge = new ArrayList<Polygon>();
    // Cache
    private int distance = 0;
    
    // Constructs a blank Edge
    public Edge(){
        for (int i = 0; i < PolyGraph.numberOfPolygons(); i++) {
        	Edge.add(null);
        }
    }
    
    // Constructs a Edge from another Edge
    public Edge(ArrayList<Polygon> Edge){
        this.Edge = (ArrayList<Polygon>) Edge.clone();
    }
    
    // Returns Edge information
    public ArrayList<Polygon> getEdge(){
        return Edge;
    }

    // Creates a random individual
    public void generateIndividual() {
        // Loop through all our destination cities and add them to our Edge
        for (int index = 0; index < PolyGraph.numberOfPolygons(); index++) {
          setPolygon(index, PolyGraph.getPolygon(index));
        }
        // Randomly reorder the Edge
        Collections.shuffle(Edge);
    }

    // Gets a Polygon from the Edge
    public Polygon getPolygon(int polyPosition) {
        return (Polygon)Edge.get(polyPosition);
    }

    // Sets a Polygon in a certain position within a Edge
    public void setPolygon(int edgePosition, Polygon poly) {
        Edge.set(edgePosition, poly);
        // If the Edges been altered we need to reset the fitness and distance
        distance = 0;
    }
    
    // Gets the total distance of the Edge
    public int getDistance(){
        if (distance == 0) {
            int EdgeDistance = 0;
            // Loop through our Edge's cities
            for (int index=0; index < EdgeSize(); index++) {
                // Get Polygon we're traveling from
                Polygon fromPolygon = getPolygon(index);
                // Polygon we're traveling to
                Polygon destinationPolygon;
                // Check we're not on our Edge's last Polygon, if we are set our 
                // Edge's final destination Polygon to our starting Polygon
                if(index+1 < EdgeSize()){
                    destinationPolygon = getPolygon(index+1);
                }
                else{
                    destinationPolygon = getPolygon(0);
                }
                // Get the distance between the two cities
                EdgeDistance += fromPolygon.findDistance(destinationPolygon);
            }
            distance = EdgeDistance;
        }
        return distance;
    }

    // Get number of cities on our Edge
    public int EdgeSize() {
        return Edge.size();
    }
    
    @Override
    public String toString() {
        String geneString = "|";
        for (int i = 0; i < EdgeSize(); i++) {
            geneString += getPolygon(i)+"|";
        }
        return geneString;
    }
}