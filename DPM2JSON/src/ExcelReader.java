import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {

	public void readFiles()
	{
		InputStream file;
		XSSFWorkbook workbook;
		XSSFSheet sheet;
		//hold x and y coordinate extract from file
		double temp;
		String workingDir = System.getProperty("user.dir");
		try {
			File inputfile= new File(workingDir+"/src/excel/"+"JC_Colorado.xlsx");
			System.out.println(inputfile.exists()); 
			file = new FileInputStream(inputfile);
			try {
				workbook = new XSSFWorkbook(file);
				sheet = workbook.getSheetAt(0);
				
				//iterate through list
				int counter = 0;
				Iterator<Row> rowIterator = sheet.iterator();
				while(rowIterator.hasNext())
				{
					ArrayList<Double> x = new ArrayList<Double>();
					ArrayList<Double> y = new ArrayList<Double>();
					Row row = rowIterator.next();
					if(row.getRowNum() == 0)
						continue;
					Iterator<Cell> cellIterator = row.cellIterator();
					// traverse cell and record coordinate
					while(cellIterator.hasNext()) 
					{
			            Cell cell = cellIterator.next();
			            //skip an not record validation index
			            if(cell.getColumnIndex() == 0 || cell.getColumnIndex() == 1)
			            	continue;
			            if(cell.getCellType() == Cell.CELL_TYPE_STRING)
			            	break;
			            temp = cell.getNumericCellValue();
			            if(temp == 0.0)
			            	break;
			            if(cell.getColumnIndex()%2==0)
			            	x.add(temp);
			            else
			            	y.add(temp);
			        }
					PolyGraph.addPolygon(new Polygon(x,y,counter));
					counter++;
				}
			} catch (IOException e) {
				System.err.println("Fail To load file");
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			System.err.println("Fail To load file");
			e.printStackTrace();
		}
	}
}

