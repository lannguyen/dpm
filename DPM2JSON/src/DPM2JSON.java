import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class DPM2JSON {
	    public static void main(String[] args) {
	    	long startTime = System.currentTimeMillis();
	    	
	        WriteReadJson wrj = new WriteReadJson();
	        wrj.readFile("");
	        DP_ON_Polygon dp = new DP_ON_Polygon();
	        
	        //this hold the index in order of shortest neighbor
	        ArrayList<Integer> orderedIndex = dp.getShortestRoute();
	        JSONArray orderedPolygon = new JSONArray();
	        for (int i = 0; i < orderedIndex.size(); i++) {
				orderedPolygon.add(wrj.arr.get(orderedIndex.get(i)));
			}
	        wrj.fixIndex(orderedPolygon);
	        orderedPolygon.add(wrj.boundaries);
	        
	        JSONObject result = new JSONObject();
	        result.put("type", "FeatureCollection");
	        result.put("features", orderedPolygon);
	        
	        wrj.out(result);
	        long endTime = System.currentTimeMillis();
	        long duration = endTime - startTime;
	        
	        System.out.println("Total Execution Time is (sec) :"  + duration/ 1000);
	        
	    }
}

