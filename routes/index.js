var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res) {	
  	res.sendfile('public/static/login.html');
});

router.get('/login',function(req,res){
    res.sendfile('public/static/login.html');
});
router.get('/map',function(req,res){
    res.sendfile('public/static/map.html');
});
router.get('/register',function(req,res){
    res.sendfile('public/static/register.html');
});
router.get('/Test',function(req,res){
    res.sendfile('public/static/Test.html');
});
router.get('/logout',function(req,res){
	req.session = null;
    res.sendfile('public/static/logout.html');
});
router.get('/admin',function(req,res){
	if(req.session.email == "admin")
    	res.sendfile('public/static/admin.html');
    else
    	res.send("<H1>You don't have permission to visit this link<H1>");
});

router.post('/upload',function(req,res){
    if(req.files)
        res.send('success');
    else
        res.send('Error Encounter');
});



/* GET home page. */

module.exports = router;
