var express = require('express');
var router = express.Router();
var json2csv = require('nice-json2csv');
router.use(json2csv.expressDecorator);


var dataHeld, array=[];


// return general user informtion
router.get('/', function(req, res) {
    var db = req.db;
    var collection = db.get('user');
    collection.find({},{},function (e, docs){
    	var response = {
			"data": docs,
			"error": null
    	};
    	res.contentType('application/json');
    	var json = JSON.stringify(response);
        dataHeld = docs;
    	res.send(json);
    });
});

//return csv file on download
router.get('/user_cvs', function(req, res) {
    array = new Array();
    function generateCVS(dataHeld)
    {
        function compare(a,b) {
            return a.i-b.i;
        }

         
        for(var i = 0; i < dataHeld.length; i++)
        {
            var user = dataHeld[i];
	    if (typeof user.data == "undefined")
		continue;
            var dataArray = user.data.sort(compare);
            var userData = {
                "email": user.Email,
            }
            array.push(userData);
            for(var j = 0; j < dataArray.length; j++){
                var data = {
                    "i": dataArray[j].i,
                    "c": dataArray[j].c
                }
                array.push(data);
            }
            dataArray = null;
        }
    }   
    generateCVS(dataHeld);
    res.csv(array,"data.csv");
});

//////////////////////////////////////////////////////////////////////
//  Login mechanism if admin user send respone to client 
//  Client will then redirect to user to admin page                                                      
//                                                                  
//////////////////////////////////////////////////////////////////////
router.post('/auth', function(req, res) {
    var db = req.db;
    var collection = db.get('user');
    var userEmail = req.body.email;
    var userPassword = req.body.password;
    if(userEmail == "admin" && userPassword == "admin"){
        req.session.email = "admin";
        res.send("admin");
    }
    else {
        collection.findOne({
            "Email":userEmail,
            "Password":userPassword
        },function (err, doc) {
            if(err){
                console.log("error");
                res.send(err)
            }
            else{
                if(doc == null)
                    res.send("fail");
                else{
                    console.log("success");
                    req.session.userID = doc._id;
                    res.send("pass");
                }
            }
        });
    }
});

//////////////////////////////////////////////////////////////////////
//  check if email is valid, respone to user with approriate tag                                                  
//                                                                  
//////////////////////////////////////////////////////////////////////

router.post('/checkEmail', function(req, res) {
    var db = req.db;
    var collection = db.get('user');
    var userEmail = req.body.email;
    collection.findOne({
        "Email":userEmail
    },function (err, doc) {
        if(err){
            //error while adding data to db
            res.send(err)
        }
        else{
            if(doc == null)
                res.send("fail");
            else{
                res.send("pass");
            }
        }
    });
});

//////////////////////////////////////////////////////////////////////
//  add user mechanism                                                     
//                                                                  
//////////////////////////////////////////////////////////////////////

router.post('/adduser', function(req, res) {
    var db = req.db;
    var userEmail = req.body.email;
    var userPassword = req.body.password;
    var userData = req.body.data;
    console.log(req.body.email,req.body.password);
    var collection = db.get('user');
    collection.insert({
    	"Email":userEmail,
    	"Password":userPassword
    },function (err, doc) {
    	if(err){
    		//error while adding data to db
    		res.send("there was a problem with database currently");
    	}
    	else{
    		res.send("Sucessfully Added");
    	}
	});
});

//////////////////////////////////////////////////////////////////////
//  remove user, this method is not used yet. Created for testing 
//  purpose only                                                    
//                                                                  
//////////////////////////////////////////////////////////////////////

router.post('/removeuser', function(req, res) {
    var db = req.db;
    var userEmail = req.body.email;
    var userPassword = req.body.password;
    console.log(req.body.email,req.body.password);
    var collection = db.get('user');
    collection.remove({
    	"Email":userEmail,
    	"Password":userPassword
    },function (err, doc) {
    	if(err){
    		//error while adding data to db
    		res.send("there was a problem with database currently");
    	}
    	else{
    		res.send("Sucessfully Added");
    	}
	});
});



module.exports = router;
