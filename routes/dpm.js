var express = require('express');
var router = express.Router();

//write to database from server side
router.post('/writeData', function(req, res) {
    console.log("in here");
    var db = req.db;
    var collection = db.get('user');
    var id = req.session.userID;
    var index = req.body.data.i;
    var color = req.body.data.c;
    collection.update({_id: id, "data.i": index},{
        $set: {"data.$.c":color}      
    },function (err, doc) {
    	if(err){
    		//error while adding data to db
            console.log(err);
    		res.send("Update Fail");
    	}
    	else{
            if(doc == 0)
            {
                collection.update({_id: id},{$push: 
                    {
                        "data" : { 
                            "i":index,
                            "c":color
                        }
                    }},function (err,doc){
                        if(err){
                            console.log(err);
                            console.log("fail to write");
                        }
                        else{
                            console.log("sucess add");
                            res.send("Success Update");
                        }
                    }
                );
            }
    	}
	});
});

router.get('/testDataLayer', function(req,res){
    res.sendfile('public/test.html');
});

//return map
router.get('/map', function(req,res){
    res.sendfile('DPM2JSON/src/data/map.json');
});

//return ordered map
router.get('/map_converted', function(req,res){
    res.sendfile('DPM2JSON/src/data/map_converted.json');
});

//return user data
router.get('/getData', function(req,res){
    var id = req.session.userID;
    if(typeof id !== "undefined"){
        var db = req.db;
        var collection = db.get('user');
        collection.findById(id,function (err,docs){
            if(err){
                res.send(err)
            }
            else{
                var response = {
                    "data" : docs.data,
                    "error": null
                };
                res.contentType('application/json');
                var json = JSON.stringify(response);
                res.send(json);
            }
        });
    }
    else
        res.send("loginRequire");
});


module.exports = router;
