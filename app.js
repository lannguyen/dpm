var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session')
var bodyParser = require('body-parser');
var host = require('os');
var multer = require('multer');

console.log(host.type());

//set up database
var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/GD');

//set up routes
var routes = require('./routes/index');
var users = require('./routes/users');
var dpm = require('./routes/dpm');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({secret: "keyboard cat",cookie:{maxAge: 864000}}));
    app.use(multer({
        dest : './public/upload/',
        rename: function(fieldname, filename){
            return filename.replace(/\W+/g, '-').toLowerCase(); 
        }
    }));
app.use(express.static(path.join(__dirname, '/public')));


//set databse for passing to routes
app.use(function (req,res, next) {
    req.db = db;
    next();
});

app.use('/', routes);
app.use('/user', users);
app.use('/dpm', dpm);

/// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
