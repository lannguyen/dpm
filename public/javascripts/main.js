requirejs.config({
	baseUrl: 'javascripts',
	paths:{
		jquery: 'lib/jquery.min',
		boostrap: 'lib/bootstrap.min',
		validation: 'app/validation',
		userProcessing: 'app/userProcessing',
		serveraddress: 'app/serveraddress'
	}
});

//send information such as email and password to validation module
require(['jquery','validation','errorIO','serveraddress'], function($,validation,errorIO,serveraddress) {
	var serverAddress = serveraddress+'/';
	$(document).bind('keypress', function(e) {
        if(e.keyCode==13){
             $('#sign_in').trigger('click');
             $('#register').trigger('click');
         }
    });

	$('#sign_in').click(function () {
		$email = $('#email');
		$password = $('#password');
		$error = $('#Error');
		var postData = {
			"email":$email.val(),
			"password":$password.val()
		};
		$.post(serverAddress+'user/auth',postData, function(data){
			if(data=="fail"){
				errorIO.errorOut($error,"Email or password doesn't regconize!.");
			}
			else if(data == "admin"){
				window.location = serverAddress+"admin"
			}
			else{
				window.location = serverAddress+"Test";
			}
		});
	});
	
	$('#email').click(function () {
			$('#emailError').addClass("hidden");
	});
	
	$('#password').click(function () {
			$('#passwordError').addClass("hidden");
	});
});

//send information such as email and password to userProcessing module
require(['jquery','validation','errorIO','userProcessing','serveraddress'], function($,validation,errorIO,userProcessing,serveraddress) {
	var serverAddress = serveraddress+'/';


	$('#register').click(function () {
		var pass1 = $('#password').val();
		var pass2 = $('#passwordConfirm').val();
		$emailError = $('#emailError');
		$passwordError = $('#passError');
		$email = $('#email');
		$password = $('#password');

		var result = validation.checkCredential($('#email').val(), $('#password').val());
		if (!result.email) {
			 	errorIO.errorOut($emailError,"Email is invalid");
			}
		else if((pass1.localeCompare(pass2)) == 0 && pass1.length > 0)
		{
			var postData = {
				"email":$email.val()
			};
			$.post(serverAddress+'user/checkEmail',postData, function(data){
				if(data==="pass")
					errorIO.errorOut($emailError,"Email Already Exist");
				else{
					userProcessing.adduser(serverAddress,$('#email').val(), $('#password').val());
				}
			});	
		}
		else
		{
			errorIO.errorOut($passwordError,"Either password doesn't match or empty");
		}	
	});
	
	$('#email').click(function () {
			$('#emailError').addClass("hidden");
	})
	
	$('#password').click(function () {
			$('#passwordError').addClass("hidden");
	})	
});

define('errorIO',function () {
	return{
		errorOut: function (element,error) {
			element.html(error);
			element.css({color: "red"});
			element.removeClass("hidden");	
		}	
	}
});


