requirejs.config({
	baseUrl: 'javascripts',
	paths:{
		jquery: 'lib/jquery.min',
		boostrap: 'lib/bootstrap.min',
		upload: 'app/upload',
		validation: 'app/validation',
		userProcessing: 'app/userProcessing',
		serveraddress: 'app/serveraddress',
		drawTable: 'app/drawTable'
	}
});


//////////////////////////////////////////////////////////////////////
//  draw the data chart after the result are avalaible                                                    
//                                                                  
//////////////////////////////////////////////////////////////////////

define('render',['jquery','serveraddress','drawTable'],function ($,serveraddress,drawTable) {
	var userdata;
	var display = "";
	$.get(serveraddress+"/user").done(function(result){
			userdata = result.data;
	});
	return {
		renderChart:function () {
				drawTable.drawCumulativeData(userdata);
				$('#loading').hide();
				$('#btn').removeClass('hide');
			}
		}	
	}         
);

require(['jquery','serveraddress'],function ($,serveraddress){
	function setTimer() {
        timerId = setInterval(function () {
            if ($('#userFileInput').val() !== '') {
                clearInterval(timerId);
                $('#uploadForm').submit();
            }
        }, 500);
    }

    function status(message)
	{
		$('#status').text(message);
	}

	// function setProgress(percent) {
 //        $('#percent').html(percent + '%');
 //        $('#bar').css('width', percent + '%');
 //    }

    setTimer();

	$('#uploadForm').submit(function () {
		var formData = new FormData();
        var file = document.getElementById('userFileInput').files[0];
        formData.append('userFile', file);
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType('application/json');
        xhr.open('post', serveraddress+'/upload', true);

        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable)
                console.log(Math.round((e.loaded / e.total) * 100));
        };

        xhr.onerror = function (e) {
            status('error while trying to upload');
        };

        xhr.send(formData);
        return false; // no refresh
	});
});

  	
require(['render','jquery'],function(render,$){
	setTimeout(render.renderChart, 5000);
});

