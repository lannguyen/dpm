requirejs.config({
	baseUrl: 'javascripts',
	paths:{
		jquery: 'lib/jquery.min',
		boostrap: 'lib/bootstrap.min',
		parseJSON: 'lib/GeoJSON',
		GoogleMapsLoader: 'app/google-maps-loader',
		renderDPM: 'app/renderDPM'
	}
});

require(['GoogleMapsLoader','renderDPM','jquery'],function(GoogleMapsLoader,renderDPM, $){
		GoogleMapsLoader.done(function(){
				renderDPM.start();
			}).fail(function(){
				console.error("ERROR: Google maps library failed to load");
		});
		//on hover show text
		$("#save").hover(function() {
			$("#save").html("save");
		}, function() {
			$("#save").html("<i class=\"fa fa-save fa-lg\"></i>");
		});
		//on hover show text
		$("#exit").hover(function() {
			$("#exit").html("exit");
		}, function() {
			$("#exit").html("<i class=\"fa fa-sign-out fa-lg\"></i>");
		});

});