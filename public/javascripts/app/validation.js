define('validate',function () {
	return {
		checkEmail: function (email) {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		   return re.test(email);			
		},
		checkPassword: function (password) {
			return (password.length > 0);	
		}
	}
});

define(['jquery','validate'],function ($,validate) {
    return {
    	  	checkCredential:function (e,p) {
	    	  	return {
	        		email: validate.checkEmail(e),
	        		password: validate.checkPassword(p)
	       		}
	       	},
       		validateUser: function(data, email, password) {
				for(var i in data)
				{
					if(data[i].Email == email && data[i].Password == password)
						return true;
				}
				return false;
			}
        }
    }
);

