require({
    baseUrl: 'javascripts',
    paths:{
        drawChart: 'app/drawChart',
        dpmDataProcessing: 'app/dpmDataProcessing'    }
});


define(['jquery','parseJSON','drawChart', 'dpmDataProcessing','app/serveraddress'],function($,parseJSON, drawChart, dpmDataProcessing,serveraddress){
    //////////////////////////////////////////////////////////////////////
    //  declaring the variable that are needed,                                                     
    //  also handling shift key on hold                        
    //////////////////////////////////////////////////////////////////////
    var eventHolder, cachedGeoJson, indexHolder = 0, centerHolder, totalPolygonRemaining, previousIndex;
    var polygons = [];
    var polygonsCopied = [];
    var center, mapOption, map, map2;
    var selectedPolygon = {
        strokeColor: 'white',
        strokeWeight: 1.5
    }
    var damage = 'red';
    var fine = 'blue';
    var unknown = 'orange';
    var multiSelect = false;
    var selectedPolys = new Array();
    window.onkeydown = function(e){
        multiSelect = ((e.keyIdentifier == 'Shift') || (e.shiftKey == true))
    }
    window.onkeyup = function(e){
        multiSelect = false;
    }
    return {
        start: function(userData){

            center = new google.maps.LatLng(11,125);
            mapOption = {
                center: center,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
                zoom: 12,
                zoomControl: false,
                scaleControl: false,
                panControl:false,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.SATELLITE
            };   

            map = new google.maps.Map(document.getElementById('map'), mapOption);
            map2 = new google.maps.Map(document.getElementById('map2'), mapOption);

            loadJson(serveraddress+'/dpm/map_converted', mStyle);

            var mStyle = {
                strokeColor: 'red',
                strokeWeight: 2,
                fillColor: 'white',
                fillOpacity: 0
            };

            // option button set up
            $('#damage').click(function(event) {
                colorManagement(damage, map2);
            });

            $('#fine').click(function(event) {
                colorManagement(fine, map2);
            });

            $('#unknown').click(function(event) {
                colorManagement(unknown, map2);
            });

             // zoom_changed Event for Zoom  
            google.maps.event.addListener(map, 'zoom_changed', function () {
                z = map.getZoom();
                c = map.getCenter();
                t = window.setTimeout(function () {
                    map.setZoom(z);
                    map2.setZoom(z);
                    clearTimeout(t);
                }, 0);
                map.setCenter(c);
                map2.setCenter(c);
            });
            google.maps.event.addListener(map2, 'zoom_changed', function () {
                z = map2.getZoom();
                c = map2.getCenter();
                t = window.setTimeout(function () {
                    map2.setZoom(z);
                    map.setZoom(z);
                    clearTimeout(t);
                }, 0);
                map.setCenter(c);
                map2.setCenter(c);
            });

            google.maps.event.addListener(map, 'drag', function (event) {
                map2.setCenter(map.getCenter());
            });
            google.maps.event.addListener(map2, 'drag', function (event) {
                map.setCenter(map2.getCenter());
            });

            //keep count of the polygon that already filled
            //perform approriate event if 1 polygon is selected
            //or multiple polygons is selected
            function colorManagement(currentStyle, mapName)
            {
                //multiple
                if(selectedPolys.length > 0)
                {
                    console.log(selectedPolys);
                    $.each(selectedPolys, function(j,index){
                        if(currentStyle == mStyle && polygons[index].fillColor != 'white')
                            removefilled(index,mStyle);
                        else fill(index,currentStyle);
                    });

                    selectedPolys = new Array();
                    indexHolder = -1;
                }
                //only 1
                else if(indexHolder != -1){
                    if(currentStyle == mStyle && polygons[indexHolder].fillColor != 'white')
                        removefilled(indexHolder, mStyle);
                    else fill(indexHolder, currentStyle);
                }
                console.log(totalPolygonRemaining);
            }

            //perform filling background
            function fill(index, currentStyle){
                var filled = {
                    fillColor: currentStyle,
                    fillOpacity: 1.0,
                    strokeColor: currentStyle
                };
                dpmDataProcessing.writeData(index,currentStyle);
                polygons[index].setOptions(filled);
                polygonsCopied[index].setOptions(filled);
                totalPolygonRemaining--;
            }

            // remove filling background
            function removefilled(index, currentStyle){
                dpmDataProcessing.writeData(index,currentStyle.fillColor);
                polygons[index].setOptions(currentStyle);
                polygonsCopied[index].setOptions(currentStyle);
                totalPolygonRemaining++;
            }

            // core part of rendering, after loading json load polygon to array
            // add functionality to each polygon 
            // extract the database information to each polygon
            function loadJson(url, style)
            {
                var promise = $.getJSON(url); //same as map.data.loadGeoJson();
                promise.then(function(data){
                    cachedGeoJson = data; //save the geojson in case we want to update its values
                    totalPolygonRemaining = data.features.length;
                    var previousCompletion = dpmDataProcessing.getData();
                    //this loop extract each polygon and assign event handler
                    for(var i = 0; i < totalPolygonRemaining; i++)
                    {
                        var polygon = new GeoJSON(data.features[i].geometry, mStyle);
                        polygon.setMap(map2);
                        var newObject = $.extend({}, polygon);
                        newObject.setMap(map);
                        polygons.push(polygon);
                        polygonsCopied.push(newObject);        
                    }
                    // setting up listerner for each polygon
                    $.each(polygons, function(i, poly){   
                        google.maps.event.addListener(poly,'click',function(){
                            //handling event where using hold shift key
                            if(multiSelect){
                                if(selectedPolys.indexOf(i) == -1)
                                    selectedPolys.push(i);
                                if(indexHolder != -1 && polygons[indexHolder].strokeColor == 'white'){
                                    if(selectedPolys.indexOf(indexHolder) == -1)
                                        selectedPolys.push(indexHolder);
                                }
                            }
                            else{
                                previousIndex = indexHolder;
                                console.log(selectedPolys.length);
                                if(selectedPolys.length > 0)
                                {
                                    $.each(selectedPolys, function(j,index){
                                        if(polygons[index].fillColor == "white")
                                            polygons[index].setOptions(mStyle); 
                                    });
                                    selectedPolys = new Array();
                                }
                                if(previousIndex != -1 && polygons[previousIndex].fillColor == "white")
                                {
                                   polygons[previousIndex].setOptions(mStyle); 
                                }
                            }
                            indexHolder = i;
                            centerHolder = cachedGeoJson.features[i].properties.centroid;
                            poly.setOptions(selectedPolygon);
                            map2.setCenter(new google.maps.LatLng(centerHolder[0],centerHolder[1]));
                        });

                        google.maps.event.addListener(poly,'rightclick',function(){
                            indexHolder = i;
                            centerHolder = cachedGeoJson.features[i].properties.centroid;
                            map2.setCenter(new google.maps.LatLng(centerHolder[0],centerHolder[1]));
                            colorManagement(mStyle, map2);
                        });
                    });
        
                    // load database and filled the completed portion if needed
                    if(typeof previousCompletion != 'undefined')
                    {
                        if(previousCompletion.length != 0)
                        {
                            for(var i = 0 ; i < previousCompletion.length; i++)
                            {
                                var index = previousCompletion[i].i;
                                var color = previousCompletion[i].c;
                                if(color != 'white')
                                {
                                    totalPolygonRemaining--;
                                }
                                if(color == 'white')
                                {
                                    continue;
                                }
                                var filled = {
                                    fillColor: color,
                                    fillOpacity: 1.0,
                                    strokeColor: color
                                };
                                polygons[index].setOptions(filled);
                                polygonsCopied[index].setOptions(filled);
                            }
                        }
                    }

                    //setting up background image
                    var imageBounds = new google.maps.LatLngBounds(
                      new google.maps.LatLng(10.820867, 124.639654),
                      new google.maps.LatLng(11.390590, 125.142709));

                    backgroundOverlay = new google.maps.GroundOverlay(
                          'images/tacloban.png',
                          imageBounds);
                    backgroundOverlay.setMap(map2);
                    drawChart.drawCumulativeData(polygons, damage,fine,unknown);
                    map2.setZoom(15);
                    $('#piechart').hide();
                    $("#next").click();
                    $("#progressBar").removeClass('show').addClass('hide');
                });
            }


            function gatherPieChartAndUpdate()
            {
                drawChart.drawCumulativeData(polygons, damage,fine,unknown);
            }

            // loop through the partial array to find the next polygon that hasn't been filled
            function findNextAvailable()
            {
                var i = indexHolder;
                if(i == (polygons.length - 1))
                    i = 0;   
                for(; i < polygons.length; i++){
                    if(polygons[i].fillColor != 'white')
                        continue;
                    else{
                        indexHolder = i;
                        break;
                    }
                }
            }

            // trigger piechart to show and hide
            $('#piechart_switcher').click(function(event) {
                drawChart.drawCumulativeData(polygons, damage,fine,unknown);
                $("#piechart").fadeToggle('slow');
            }); 

            //save
            $('#save').click(function(event) {
                dpmDataProcessing.writeData(indexHolder,currentStyle);
            }); 

            //exit
            $('#exit').click(function(event) {
                var remain;
                if(totalPolygonRemaining > 0 )
                    remain = "?remain";
                else
                    remain = "";
                window.location = "logout"+remain;
            }); 

            //move to the next polygon
            $('#next').click(function(event) {
                if(totalPolygonRemaining != 0){
                    findNextAvailable(); 
                    google.maps.event.trigger(polygons[indexHolder], 'click', {});
                }
                else
                    console.log(totalPolygonRemaining);
                      
            }); 

        }
    }
});


