requirejs.config({
    baseUrl: 'javascripts',
    waitSeconds : 15, //make sure it is enough to load all scripts
    paths : {
        //alias to plugins
        async : 'lib/async',
        goog : 'lib/goog',
        propertyParser : 'lib/propertyParser',
    }
});


define(['goog!visualization,1,packages:[corechart,geochart]', 'goog!search,1'], function(){
    // code copied from google charts docs:
    // http://code.google.com/apis/chart/interactive/docs/gallery/piechart.html
    options = {
                'title': 'DPM Validation Status',
                'colors': ['red', 'blue', 'orange', 'green'],
                'titleTextStyle': {
                    color: 'white'
                },
                'chartArea': {
                    left: 10,
                    top: 20,
                    width: '100%',
                    height: '100%'
                },
                pieSliceBorderColor : "white",
                "backgroundColor": { fill: "none" },
                'fontSize': 13,
                'width': 300,
                'height': 210,
                'backgroundColor': {
                    'fill': 'white',
                    'fillOpacity': 0.3
                },
                animation:{
                    duration: 1000,
                    easing: 'out'
                },
                is3D: false,
                legend: {
                    position: 'right',
                    alignment: 'center',
                    textStyle: {
                        color: 'white',
                        fontsize: 12
                    }
            }
    };
        return {
          drawCumulativeData: function(polygons,damage,fine,unknown) 
          {
            var numDamage = 0, numFine = 0, numUnknown = 0;
            for(var i = 0; i < polygons.length; i++)
            {
                if(polygons[i].fillColor == damage)
                    numDamage++;
                else if(polygons[i].fillColor == fine)
                    numFine++;
                else if(polygons[i].fillColor == unknown)
                    numUnknown++;
            }
            var totalPolygonRemaining = polygons.length - (numDamage+numFine+numUnknown);

            dataSet =   [
                          ['Task', '#Of Polygon per validation'],
                          ['Damage',     numDamage],
                          ['Undamage',   numFine],
                          ['Uncertain',  numUnknown],
                          ['Remain',    totalPolygonRemaining]
                        ]
            var data = new google.visualization.arrayToDataTable(dataSet);
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
          }
      }
});