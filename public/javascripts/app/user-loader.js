var user_loaded_def = null;

define(['jquery'],function($) {

  if(!user_loaded_def) {

    user_loaded_def = $.Deferred();

    window.user_loaded = function() {
      user_loaded_def.resolve(user.load);
    }

    require(['https://localhost:3000/user?&format=json'],function(){},function(err) {
      user_loaded_def.reject();
      console.log("Error");
      //throw err; // maybe freak out a little?
    });

  }

  return user_loaded_def.promise();

});
