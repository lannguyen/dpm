requirejs.config({
    baseUrl: 'javascripts',
    waitSeconds : 15, //make sure it is enough to load all scripts
    paths : {
        //alias to plugins
        async : 'lib/async',
        goog : 'lib/goog',
        propertyParser : 'lib/propertyParser',
    }
});


define(['goog!visualization,1,packages:[corechart,geochart]', 'goog!search,1'], function(){
    // code copied from google charts docs:
    // http://code.google.com/apis/chart/interactive/docs/gallery/piechart.html
    options = {
                'title': 'DPM Validation Status',
                'colors': ['red', 'blue', 'orange'],
                 vAxis: {title: 'user',  titleTextStyle: {fontSize: '25px', bold: true, color: 'black'}}, 
                 hAxis: {title: 'number of polygons',  titleTextStyle: {fontSize: '25px', bold: true, color: 'black'}},   
                pieSliceBorderColor : "white",
                "backgroundColor": { fill: "none" },
                'backgroundColor': {
                    'fill': 'white',
                    'fillOpacity': 0.4
                }
            }
        return {
          drawCumulativeData: function(data) 
          {

            dataSet =   [
                          ['Email', 'damaged', 'undamaged', 'unknown']
                        ]

            function generateRow(person){
                if (typeof person == 'undefined' || typeof person.data == 'undefined' )
                    return "invalid";
                var result = new Array();
                var red = 0,blue = 0,orange = 0,white = 0;
                for(var i = 0; i < person.data.length; i++)
                {
                    polygon = person.data[i].c;
                    if(polygon == "#FF0000" || polygon == 'red')
                        red++;
                    else if (polygon == "#0000E6" || polygon == 'blue')
                        blue++;
                    else if (polygon == "FF6600" || polygon == 'orange')
                        orange++;
                }
                result.push(person.Email);
                result.push(red);
                result.push(blue);
                result.push(orange);

                return result;
            }

            for(var i = 0; i < data.length; i++)
            {
                var result = generateRow(data[i]);
                if(result == 'invalid')
                    continue;
                dataSet.push(result);
                console.log(dataSet);
            }
            var data = new google.visualization.arrayToDataTable(dataSet);
            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
            chart.draw(data, options);  
          }
      }
});