define(['jquery'],function ($) {
	return {
		adduser: function(serverAddress, email, password) {
			var postData = {
				"email":email,
				"password":password,
				"data": []
			};
			//add user to database from client side
			$.post(serverAddress+"user/adduser",postData, function (data) {
				console.log(data);
				if(data=="error")
				{
					$("#loginModal").removeClass('show');
					$("#output").html("Error with database");
					$("#textError").html("You might want to try again.");
					$("#redirect").val("Register");
					$("#redirect").click(function(){
						window.location = serverAddress+"register";
					});
					$("#outputModal").removeClass('hide').addClass('show');
				} // if no error display sucessful created account
				else 
				{
					$("#registerModal").removeClass('show');
					$("#output").html("Account Succesfully created");
					$("#redirect").val("Return to Login page");
					$("#redirect").click(function(){
						window.location = serverAddress+"login";
					});
					$("#outputModal").removeClass('hide').addClass('show');
				}
			});
		},
		checkuser: function(data, email) {
			for(var i in data)
			{
				if(data[i].Email == email)
					return true;
			}
			return false;
		}
	}	
});

