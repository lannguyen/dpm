//////////////////////////////////////////////////////////////////////
//  perform data processing, after geting the data from server
//	assign data to holder, also perform updating data to server via writeData method                                                    
//                                                                  
//////////////////////////////////////////////////////////////////////

define(['jquery','app/serveraddress'],function ($,serveraddress) {
	var completedMap;
	var serverAddress = serveraddress;
	$.get(serverAddress+"/dpm/getData").done(function(result){
		if(typeof result.data == "undefined"){
			completedMap = [];
		}
		if(completedMap == "loginRequire")
			window.location = "login";
		else completedMap = result.data;
	});
    return {
    	  	getData:function () {
    	  		return completedMap;
	       	},
       		writeData: function(index,color) {
       			var data = {
					'i' : index,
					'c' : color
				}
				$.post(serverAddress+'/dpm/writeData',{"data":data}, function(result){
				if(result=="Update Fail")
					console.log('fail');
				else
					console.log('success');
				});
			}
        }
    }
);